var tituloEvento=document.getElementById("tituloEvento");
var fecha=document.getElementById("fecha");
var botonEnviar=document.getElementById("enviarE");
 botonEnviar.addEventListener("click",crearEvento);
var contador=0;
function crearEvento(){
  var contenedorEntradas=document.getElementById("contenedorEntradas");
  var entradaNueva=document.createElement("article");
    entradaNueva.setAttribute("class","row");
  var columnaTarjeta=document.createElement("div");
    columnaTarjeta.setAttribute("class","col s12");
  var contenedorTarjeta=document.createElement("div");
    contenedorTarjeta.setAttribute("class","card texto--rosa roboto-cond");
  var contenidoTarjeta=document.createElement("div");
    contenidoTarjeta.setAttribute("class","card-content texto--rosa roboto-cond");
  var tituloTarjeta=document.createElement("h4");
    tituloTarjeta.setAttribute("class","card-title ");
  var fechaTarjeta=document.createElement("span");
  var mapaDiv=document.createElement("div");

  contenidoTarjeta.appendChild(mapaDiv);
  insertarContenidoEvento(tituloTarjeta,fechaTarjeta,contenidoTarjeta)
  crearEntradaNueva(contenidoTarjeta,contenedorTarjeta,columnaTarjeta,entradaNueva);
  agregarEntradaAContenedor(contenedorEntradas,entradaNueva);
      mapaDiv.setAttribute("class","timeNow");
      mapaDiv.style.width= "100%";
      mapaDiv.style.height= "400px";
  initMap();
  contador++;
  tituloEvento.value="";
  fecha.value="";

}

function initMap() {
          var map = new google.maps.Map(document.getElementsByClassName("timeNow")[contador], {
          center: {lat: -34.397, lng: 150.644},
          zoom: 18
        });
        var infoWindow = new google.maps.InfoWindow({map: map});

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
      infoWindow.setPosition(pos);
      infoWindow.setContent(browserHasGeolocation ?
                                'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
      }


function insertarContenidoEvento(tituloTarjeta,fechaTarjeta,contenidoTarjeta){
  tituloTarjeta.innerHTML=tituloEvento.value;
  fechaTarjeta.innerHTML=fecha.value;

  contenidoTarjeta.appendChild(tituloTarjeta);
  contenidoTarjeta.appendChild(fechaTarjeta);
}
